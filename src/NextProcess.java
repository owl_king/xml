import java.util.ArrayList;


public class NextProcess {
	public static  ArrayList<ClassInfo> arrClassInfo = new  ArrayList<ClassInfo>();
	public void getArrClassInfo(ArrayList<String> impString) {
		int k=0;
		while (k<impString.size()) {
			if(impString.get(k).contains(" class ") || impString.get(k).indexOf("class ")==0) {
				ArrayList<Met_ProInfo> arrMet = new ArrayList<Met_ProInfo>();
				ArrayList<Met_ProInfo> arrPro = new ArrayList<Met_ProInfo>();
				Process process = new Process();
				process.getClass(impString.get(k));
				//System.out.println(process.className);
				if(impString.size()==1 ||k==impString.size()-1) {
					arrClassInfo.add(new ClassInfo(process.className, process.extClassName, arrMet, arrPro));
					break;
				}
				else {
					while(!impString.get(k+1).contains(" class ") && impString.get(k+1).indexOf("class ")!=0) {
						if(impString.get(k+1).contains("(") && !impString.get(k+1).contains(" new ")) {
							arrMet.add(new Met_ProInfo(process.getMethod(impString.get(k+1)).modifier, process.getMethod(impString.get(k+1)).datatype, process.getMethod(impString.get(k+1)).name));
						}
						else {
							arrPro.add(new Met_ProInfo(process.getProperty(impString.get(k+1)).modifier, process.getProperty(impString.get(k+1)).datatype, process.getProperty(impString.get(k+1)).name));
//							System.out.println(process.getProperty(impString.get(k+1)).name+ "     property");
						}
						k++;
						if(k==(impString.size()-1)){
							break;
						}
							//break;  // break out this while!
					}
				}
				//System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
				arrClassInfo.add(new ClassInfo(process.className, process.extClassName, arrMet, arrPro));
				k++;
			}
		}
		
		//return arrClassInfo;
	}
//***********************************************************************************************************//	
}
