import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JComponent;


public class Class_Graphic extends JComponent  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int height_Met;
	private int height_Pro;
	public Point pos;
	public static final int WIDTH = 250; 
	public static final int HEIGHT = 30;
	public static final int WIDTH_CHARACTER = 6;
	public static final int HEIGHT_CHARACTER = 25;
	public static final int START_POS = 20;					// Starting first string position from Rect coordinates
	public static final int END_POS = 10;
	
	public ClassInfo classInfo;
	public Class_Graphic(ClassInfo classInfo, Point pos){
		
		height_Met = classInfo.methodInfo.size() + 1;
		height_Pro = classInfo.propertyInfo.size() + 1;
		this.classInfo = classInfo;
		this.pos = pos;
	}
	public void paintComponent(Graphics g){
		super.paintComponents(g);
		
		//Draw Class
		g.drawRect(pos.x ,pos.y , WIDTH , HEIGHT);
		g.drawString(classInfo.nameClass,pos.x  + (WIDTH/2) - classInfo.nameClass.length()*WIDTH_CHARACTER/2, pos.y + START_POS);
		
		//Draw Property
		if(height_Pro == 1){
			g.drawRect(pos.x ,pos.y  + HEIGHT, WIDTH , HEIGHT);
		}
		else{
			g.drawRect(pos.x ,pos.y  + HEIGHT, WIDTH , START_POS + HEIGHT_CHARACTER * (height_Pro-2) + END_POS);		
			for(int i = 0 ; i<height_Pro-1 ; i++){
				String z = classInfo.propertyInfo.get(i).modifier + "   " + classInfo.propertyInfo.get(i).datatype + "   " + classInfo.propertyInfo.get(i).name;
				g.drawString(z,pos.x + 20 ,pos.y  + START_POS + HEIGHT + i*HEIGHT_CHARACTER );
			}
		}
		//Draw Method
		if(height_Met == 1 && height_Pro == 1){
			g.drawRect(pos.x ,pos.y  + HEIGHT + HEIGHT, WIDTH , HEIGHT);
		}
		else if(height_Met == 1 && height_Pro != 1){
			g.drawRect(pos.x ,pos.y  + HEIGHT + START_POS + HEIGHT_CHARACTER * (height_Pro-2) + END_POS, WIDTH , HEIGHT);
		}
		else if(height_Met != 1 && height_Pro == 1){
			g.drawRect(pos.x ,pos.y  + HEIGHT + HEIGHT, WIDTH , START_POS + HEIGHT_CHARACTER * (height_Met-2) + END_POS);
			for(int i = 0 ; i<height_Met-1 ; i++){
				String z = classInfo.methodInfo.get(i).modifier + "   " + classInfo.methodInfo.get(i).datatype + "   " + classInfo.methodInfo.get(i).name;
				g.drawString(z,pos.x + 20 ,pos.y  + HEIGHT + HEIGHT + START_POS + HEIGHT_CHARACTER * (height_Pro-2) + END_POS + START_POS 	+ i*HEIGHT_CHARACTER);
			}
		}
		else{
			g.drawRect(pos.x ,pos.y  + HEIGHT + START_POS + HEIGHT_CHARACTER * (height_Pro-2) + END_POS, WIDTH, START_POS + HEIGHT_CHARACTER * (height_Met-2) + END_POS);
			for(int i = 0 ; i<height_Met-1 ; i++){
				String z = classInfo.methodInfo.get(i).modifier + "   " + classInfo.methodInfo.get(i).datatype + "   " + classInfo.methodInfo.get(i).name;
				g.drawString(z,pos.x + 20 ,pos.y  + HEIGHT + START_POS + HEIGHT_CHARACTER * (height_Pro-2) + END_POS + START_POS 	+ i*HEIGHT_CHARACTER);
			}
		}
	}
}
