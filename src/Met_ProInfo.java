
public class Met_ProInfo {
	public String modifier;
	public String datatype;
	public String name;
	public Met_ProInfo() {
		modifier = null;
		datatype = null;
		name = null;
	}
	public Met_ProInfo(String modifier, String datatype, String name) {
		this.modifier = modifier;
		this.datatype = datatype;
		this.name = name;
	}
}
