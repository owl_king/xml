

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;


public class Read_String {
	private String line=null; // each lines in JAVA file.
	private Scanner s;
	private String trimspace(String str) {
	     str = str.replaceAll("\\s+", " ");
	     str = str.replaceAll("(^\\s+|\\s+$)", "");
	     return str;
	}
	
	private ArrayList<String> impString; // the most important string, about classes, methods, properties.
	public ArrayList<String> getImpString(String linkFile) {
		impString = new ArrayList<String> ();
		try {
			s=new Scanner(new BufferedReader(new FileReader(linkFile)));
			
			while(s.hasNext()) {
				line=s.nextLine();
		
				if(line.contains("//")) {
					line=line.replaceAll("//.*", "");
				}
				if(line.contains("/*") && line.contains("*/")) {
					line=line.replaceAll("/\\*.*\\*/", "");
				}
				//remove single comments.
				if(line.contains("/*")) {
					line=s.nextLine();
					while (!line.contains("*/")) {
						line=s.nextLine();
					}
				}
				if(line.contains("*/")) {
					line="";
				}
				//remove multi comments
				
				//***********************************************************************************************//
				line= trimspace(line.replace("{", "").replace("}", "").replace(";", ""));
				//remove space, remove "{" or "}" or ";"
				//***********************************************************************************************//
				if(!line.contains("\"") && !line.contains(" main(") && !line.contains(" main (")) { 
					if(line.contains(" class ") || line.indexOf("class ")==0 || line.contains("public ") || line.contains("protected ") || line.contains("private ")) {
						impString.add(line);
					}
				}
				//***GET THE MOST IMPORTANT STRING INFO
				//***********************************************************************************************//				
				//System.out.println(line);
			}		
		}
		catch (FileNotFoundException e) {
			System.out.println("File not found!");
		}
		return impString;
	}
	
}

