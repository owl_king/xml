
public class Process {
	public String className = null;
	public String extClassName = null;
	public Met_ProInfo me = new Met_ProInfo();
	public Met_ProInfo pr = new Met_ProInfo();
	
	public void getClass(String line){
		//public class A
		//abstract public class A
		//public abstract class A
		//public class A extends B
		//class A
		
		String [] a = line.split(" ");
		
		int i = 1;
	
		if(a[0].equals("abstract")||a[1].equals("abstract")){					//co " abstract " thi ten class lui lai 1
			i++; 
		} 
		else{											
			if(line.startsWith("class"))
				i--;
				
			if(line.contains(" extends ")){
				extClassName = a[i+3];
			}
		}
		
		className = a[i+1];
		
		//return (className + "\t" + extClassName);
	}
	
	public Met_ProInfo getMethod(String line){
		//public classname() 
		//public void func() 
		//abstract public void func() 
		//public void func() throws Exception 
		
		int i = 0;
		
		if(line.startsWith("abstract")) 											// "abstract" o dau thi tang bien dem
			i++;
		
		String [] a = line.split(" ", i + 3);
		
		if(a[i].equals("public"))
			me.modifier = "+";
		else if(a[i].equals("private"))
			me.modifier = "-";
		else
			me.modifier = "#";
		
		String cls = className;
		if (line.contains(" " + cls + " ")||line.contains(" " + cls + "(")){
			String  [] b = line.split(" ", 2);
			me.datatype = null;
			me.name = b[1];
			//return new met_pro_Info (me.modifier, me.datatype, me.name);
			return me;
		}
		
		
		me.datatype = a[i+1];
		me.name = a[i+2];
		
		if (me.name.contains(" throws ")) {					// neu ten co " throws " thi cat toi " throws "
			String s = me.name.substring(0, me.name.indexOf("throws"));
			me.name = s;
		}
		return me;
	}
	
	public Met_ProInfo getProperty(String line){
		//public int a
		//public int a = 100
		//public static final int a
		//public static int a = 100
		//public final int a = 100
		int i = 1;
		
		if(line.contains(" final ")&&line.contains(" static "))			//neu co ca " final " va " static " thi kieu bien va ten bien lui lai 2
			i += 2;
		
		else if(line.contains(" final ")||line.contains(" static "))		//neu chi co 1 trong 2 thi ten bien lui lai 1
			i += 1;
		
		String [] a = line.split(" ", i + 2);
		
		if(a[0].equals("public"))
			pr.modifier = "+";
		else if(a[0].equals("private"))
			pr.modifier = "-";
		else
			pr.modifier = "#";
			
		pr.datatype = a[i];
		pr.name = a[i+1];
		
		if(pr.name.contains("=")){										//neu ten bien co "=" thi cat toi ky tu "="
			String s = pr.name.substring(0, pr.name.indexOf("="));
			pr.name = s;
		}
		
		return pr;
	}
	
}
//***********************************************************************************************************//	
//protected abstract void endMonthCharge();
//public final PhanSo divide (final PhanSo a) throws Exception
//somethings like that

