import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JComponent;


public class Content extends JComponent{
	private static final long serialVersionUID = 1L;

	public Content(int w,int h)
	{
		this.setPreferredSize(new Dimension(w,h));
	}
}
