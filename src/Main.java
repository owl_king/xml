import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.swing.*;

public class Main extends JFrame implements ActionListener{
	/**
	 * 
	 */
	File linkOfFolder;
	JButton openButton;
    JTextArea log;
    JFileChooser fc;
    JFrame background;
    public static boolean checkFolderOpen = false;
	public Main(){
		background = new JFrame("Kingdz");
		background.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		openButton = new JButton("Open Folder");
		openButton.setSize(new Dimension(200,40));
		openButton.setLocation(50,100);//position of Button
		openButton.addActionListener(this);
		Content content=new Content(500	,500);
		background.add(content);
		background.pack();
		background.setVisible(true);
		
		content.add(openButton);
		
	}
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		  JFileChooser chooser;
		  String choosertitle = null;
		  chooser = new JFileChooser(); 
		  chooser.setCurrentDirectory(new java.io.File("."));
		  chooser.setDialogTitle(choosertitle);
		  chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		  chooser.setAcceptAllFileFilterUsed(false);
		  //    
		  if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			  linkOfFolder = chooser.getSelectedFile();
			  checkFolderOpen = true;
			  }
		  else 
			  System.out.println("No Selection");
		
	}	
	
	public static void main(String args[]) throws InterruptedException{
		Main a = new Main();
    	while(true){
    		TimeUnit.MILLISECONDS.sleep(500);//delay 1/2 second
    		if(a.checkFolderOpen){
    			Read_Folder rf = new Read_Folder(a.linkOfFolder.getAbsolutePath());
    			ArrayList<String> javaFiles = rf.getListFiles();
    			Read_String rs = new Read_String();
    			//rs.getImpString("D:/Store.java");
    			NextProcess np = new NextProcess();
    			for(int i=0; i<javaFiles.size(); i++) {
    				np.getArrClassInfo(rs.getImpString(javaFiles.get(i)));
    			}
    			
    			
    			ArrayList<Class_Graphic> classGraphic = new ArrayList<Class_Graphic>();
    			for(int i=0; i<NextProcess.arrClassInfo.size(); i++) {
    				classGraphic.add(new Class_Graphic(NextProcess.arrClassInfo.get(i), new Point (0,0)));
    			}

    			ArrangementsClass background2 = new ArrangementsClass(classGraphic);
    			
    			a.checkFolderOpen=false;
    			
    		}
		}
    	
		
	}
}
