import java.util.ArrayList;


public class ClassInfo {
	public String nameClass;
	public String extClass;
	public ArrayList<Met_ProInfo> methodInfo;
	public ArrayList<Met_ProInfo> propertyInfo;
	public ClassInfo() {
		nameClass = null;
		extClass = null;
		methodInfo = new ArrayList<Met_ProInfo>();
		propertyInfo = new ArrayList<Met_ProInfo>();
	}
	public ClassInfo(String nameClass, String extClass, ArrayList<Met_ProInfo> methodInfo, ArrayList<Met_ProInfo> propertyInfo) {
		this.nameClass 	= nameClass;
		this.extClass 	= extClass;
		this.methodInfo = methodInfo;
		this.propertyInfo = propertyInfo;
	}
}
