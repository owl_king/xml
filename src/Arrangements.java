import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class Arrangements extends JFrame{
	public Arrangements (ArrayList<Class_Graphic> classGraphic){
		JFrame background = new JFrame();
		JPanel panel = new JPanel();
		background.add(panel);
		background.setVisible(true);
		background.setSize(new Dimension(1200, 600));
		int i = 0;
		int x = 0, y;
		while(i < classGraphic.size()){
			panel.add(classGraphic.get(i)).setPreferredSize(new Dimension(251,500));
			i++;
		}
		JScrollPane jScrollPane = new JScrollPane(panel);
		background.add(jScrollPane);
		background.setLocationRelativeTo(null);
		background.setResizable(false);
	}
}
